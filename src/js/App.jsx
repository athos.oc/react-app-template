import React, { Component } from 'react';
import './App.scss'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { createBrowserHistory } from "history";

import HomePage from './pages/HomePage';



const history = createBrowserHistory();



class App extends Component {

    
    render() {

      return (
            <Router history={history}>
    
                <div>
                    <Switch>
                        <Route path="/">
                            <div className="page-container"><HomePage /></div>
                        </Route>
                    </Switch>
                </div>

            </Router>
      );
    }
  }

export default App;
